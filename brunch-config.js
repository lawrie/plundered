module.exports = {
  // See http://brunch.io for documentation.
  files: {
    javascripts: {
      joinTo: {
        'vendor.js': /^(?!app)/,
        'app.js': /^app/
      }
    },
    stylesheets: {joinTo: 'app.css'},
    templates: {joinTo: 'app.js'}
  },

  plugins: {
    babel: {presets: ['es2015', 'stage-0']},

    sass: {
      options: {
        includePaths: ['/app/']
      }
    }
  }
};
